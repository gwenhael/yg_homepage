$.modal.defaults = {
  overlay: "rgb(248,246,230)",        // Overlay color
  opacity: 0.75,          // Overlay opacity
  zIndex: 99,              // Overlay z-index.
  escapeClose: true,      // Allows the user to close the modal by pressing `ESC`
  clickClose: true,       // Allows the user to close the modal by clicking the overlay
  closeText: '×',     // Text content for the close <a> tag.
  showClose: true,        // Shows a (X) icon/link in the top-right corner
  modalClass: "modal",    // CSS class added to the element being displayed in the modal.
  spinnerHtml: null,      // HTML appended to the default spinner during AJAX requests.
  showSpinner: true       // Enable/disable the default spinner during AJAX requests.
};
