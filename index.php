<?php
$lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
if (substr($lang, 0, 2) == 'fr') {
	header("Location: index.fr.html");
} else {
	header("Location: index.en.html");
}
?>